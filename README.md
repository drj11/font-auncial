# A font: 2021 March

Modelled on a non-specific uncial.

Metrics?

Angle 36 degrees.
x-width of vertical stroke 57.
y-height of horizontal stroke 41.


# Test words

- note
- code
- mole
- joke
- wipe
- shoe
- quiz
- flux
- gaze
- brew
- vary
